# Check Serial number and Asset tag Integrity

## Popis
Tento report je určen pro kontrolu vyplnění a kontrolu duplicit sériových čísel a asset tagů zařízení jako objektů Netboxu.

## Instalace
- **[Verze 3.4.X a níže]** Report je nutno vložit do složky reports, která má nejčastěji tuto cestu netbox/reports/.
- **[Verze 3.5.X a výše]** Report je nutné naimportovat z lokální složky. Cesta v Netboxu je Customization -> Reports -> Add
- Viz https://docs.netbox.dev/en/stable/customization/reports/

## Výstup
![alt text](/images/check-serial-asset-tag-integrity.png)

## Prerekvizity
1. Vytvořen objekt Device
2. Vyplněné globální proměnné v reportu

## Postup fungování Reportu
1. Report nejprve projde všechny objekty Device po aplikování filtru.
2. Podle uživatelských nastavení proběhnou kontroly jednotlivých atributů objektu Device. V případě, že uživatel nenastavil určitou kontrolu, je o této skutečnosti informován.
3. Kontrola atributu `Serial Number` zda je tento atribut vyplněn u všech zařízení.
4. Kontrola atributu `Asset Tag` ověřuje, zda je tento atribut vyplněn u všech zařízení.
5. Kontrola duplicit ověřuje, zda objekt Device má atributy `Serial Number` unikátne v celém seznamu objektů Devices.
6. Report následně vypíše seskupené Failure logy podle kontrol
7. Na závěr v případě nastavení, se na konci odešle email adresátům o výsledku Reportu.

## Proměnné
**Pro správné fungování reportu je nutné upravit globální proměnné.**

#### CHECK_SERIAL
 - Tato proměnná signalizuje, zdali se má kontrolovat vyplnění atributu `Serial Number`.
 - Report zahlásí chybu, pokud daný kabel nemá vyplněný atribut `Serial Number`.
 - Výchozí hodnota je nastavena na True.
```python
CHECK_SERIAL = True
```

#### CHECK_ASSET_TAG
 - Tato proměnná signalizuje, zdali se má kontrolovat vyplnění atributu `Asset Tag`.
 - Report zahlásí chybu, pokud daný kabel nemá vyplněný atribut `Asset Tag`.
 - Výchozí hodnota je nastavena na True.
```python
CHECK_ASSET_TAG = True
```

#### CHECK_SERIAL_DUPLICITY
 - Tato proměnná signalizuje, zda se má kontrolovat duplicitní výskyt atributu `Serial Number` u objektů Device.
 - Report zahlásí chybu, pokud se najde zařízení s duplicitním atributem `Serial Number`.
 - Výchozí hodnota je nastavena na True.
```python
CHECK_SERIAL_DUPLICITY = True
```

#### FILTER
 - Táto proměnná představuje atributy, podle kterých se má výběr zařízení filtrovat. 
 - V případě ponechání prázdných složených závorek nebude použit žádný filtr.
 - Výchozí hodnota je nastavena na Null.
```python
FILTER = {
    "tenant__name" : "Prague",
}
```
#### JOB_TIMEOUT
- Táto proměnna představuje timeout pro běh reportu.
- Výchozí hodnota je nastavena na 300 vteřin.
```python
JOB_TIMEOUT = 300
```
#### SEND_EMAIL
 - Tato proměnná představuje možnost posílání emailu o stavu reportu po jeho dokončení. 
 - Výchozí hodnota je nastavena na False.
 ```python
SEND_EMAIL = False
 ```

#### EMAIL
 - Táto proměnná představuje dictionary s parametry pro správné odesílání emailů. 
 - V kódu reportu se skládá z následujících parametrů:
    - **subject** - Předmět emailu
    - **from_email** - Odesílatel emailu
    - **recipient_list** - list příjemců mailu
    - Další parametry se nacházejí v konfiguraci NetBoxu v souboru configuration.py 
        - Dokumentace: **https://docs.djangoproject.com/en/4.2/topics/email/**
```python
EMAIL = {
    'subject': 'NetBox: Check Serial number and Asset tag Integrity Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "user@domain.com"
    ],
}
```

#### EMAIL_FORMAT
 - Tato proměnná představuje formátování pro zaslaný email.
 - Pro zpracování je použita šablona se syntaxi Jinja v Django provedení.
 - Výsledek reportů je do šablony vložen přes proměnnou s názvem `data`.
```python
EMAIL_FORMAT = """

"""
```
_**Vzorový email**_
```python
Test_Asset_Tag:
            INFO(0)
                   
            SUCCESS(0)
                   
            FAILURE(8)
               
                    device1  -  /dcim/devices/10/
               
                    device2  -  /dcim/devices/3/
               
                    device3  -  /dcim/devices/7/
               
                    device4  -  /dcim/devices/2/
               
                    device5  -  /dcim/devices/1/
               
                    device6  -  /dcim/devices/9/
               
                    device7  -  /dcim/devices/4/
               
                    device10  -  /dcim/devices/12/
               
            WARNING(0)
               

Test_Serial:
            INFO(0)
                   
            SUCCESS(0)
                   
            FAILURE(8)
               
                    device1  -  /dcim/devices/10/
               
                    device2  -  /dcim/devices/3/
               
                    device3  -  /dcim/devices/7/
               
                    device4  -  /dcim/devices/2/
               
                    device5  -  /dcim/devices/1/
               
                    device6  -  /dcim/devices/9/
               
                    device7  -  /dcim/devices/4/
               
                    device10  -  /dcim/devices/12/
               
            WARNING(0)
               

Test_Serial_Duplicity:
            INFO(0)
                   
            SUCCESS(0)
                   
            FAILURE(0)
                   
            WARNING(0)

```

**Pro správné fungování je nutně vyplnění atributů v .env souboru "netbox.env"**
```python
EMAIL_FROM=netbox@domain.com
EMAIL_PASSWORD=
EMAIL_PORT=25
EMAIL_SERVER=relay.domain.com
EMAIL_SSL_CERTFILE=
EMAIL_SSL_KEYFILE=
EMAIL_TIMEOUT=5
EMAIL_USERNAME=
```
