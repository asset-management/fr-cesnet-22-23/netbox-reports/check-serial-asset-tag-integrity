import json

from django.core.mail import send_mail
from django.template import Template, Context

from dcim.models import Device
from extras.reports import Report

# Need to check the filled serial number
CHECK_SERIAL = True

# Need to check the filled asset tag
CHECK_ASSET_TAG = True

# Need to check duplicate serial numbers
CHECK_SERIAL_DUPLICITY = True

# Parameteres for filtering devices.
# In the case of an empty dictionary, all devices will be returned.
# -> (equal to .all() method)
FILTER = {
    #'device_role__slug': 'l3-switch',
    #'name__startswith': 'sw',
    #'serial': '123456789'
}

#Timeout for the report run
JOB_TIMEOUT = 300

# Need for sending the Email about the generated report
SEND_EMAIL = False

# Parametres for Email, if the variable SEND_EMAIL is True
EMAIL = {
    'subject': 'NetBox: Check Serial number and Asset tag integrity Report',
    'from_email': 'netbox@example.com',
    'recipient_list': [
       "mail@example.com"
    ]
}

# Template for Email written in Jinja2
EMAIL_FORMAT = """
{% for keys, values in data.items %}
{{ keys|title }}:
            INFO({{ values.info|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'info' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            SUCCESS({{ values.success|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'success' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            FAILURE({{ values.failure|safe }})
                    {% spaceless %}
                {% for name in values.log %}
                    {% spaceless %}
                        {% if name.1 == 'failure' %}
                            {{ name.2|safe }}  -  {{ name.3|safe }}
                        {% endif %}
                    {% endspaceless %}
                {% endfor %}
                    {% endspaceless %}
            WARNING({{ values.warning|safe }})
                {% spaceless %}
            {% for name in values.log %}
                {% spaceless %}
                    {% if name.1 == 'warning' %}
                        {{ name.2|safe }}  -  {{ name.3|safe }}
                    {% endif %}
                {% endspaceless %}
            {% endfor %}
                {% endspaceless %}
{% endfor %}
"""

def order_log(func):
    def wrapper(self, *args, **kwargs):
        result = {}
        func(self, result)
        for log_type in ['log', 'log_failure', 'log_warning', 'log_info', 'log_success']:
            attr = result.get(log_type)
            if attr is not None:
                for record in attr:
                    getattr(self, log_type)(*record)

    return wrapper

class DeviceReport(Report):
    description = "Check Serial number and Asset tag integrity."
    job_timeout = JOB_TIMEOUT

    @property
    def name(self):
        return "Serial number and Asset tag report."
    
    @order_log
    def test_serial(self, result):
        if not CHECK_SERIAL:
            result['log'] = result.get('log', []) + [('This test is not performed. (CHECK_SERIAL = False)', )]
            return
        
        for device in Device.objects.filter(**FILTER):
            device_serial = device.serial
            if device_serial == "":
                result['log_failure'] = result.get('log_failure', []) + [(device,f"Device doesn't have serial number.", )]
    
    @order_log
    def test_asset_tag(self, result):
        if not CHECK_ASSET_TAG:
            result['log'] = result.get('log', []) + [('This test is not performed. (CHECK_ASSET_TAG = False)', )]
            return
        
        for device in Device.objects.filter(**FILTER):
            device_asset = device.asset_tag

            if device_asset is None:
                result['log_failure'] = result.get('log_failure', []) + [(device,f"Device doesn't have asset tag.", )]

    @order_log
    def test_serial_duplicity(self, result):
        if not CHECK_SERIAL_DUPLICITY:
            result['log'] = result.get('log', []) + [('This test is not performed. (CHECK_SERIAL_DUPLICITY = False)', )]
            return

        dict_of_serial = {}

        for device in Device.objects.filter(**FILTER):
            if device.serial == "":
                continue
            elif device.serial in dict_of_serial:
                dict_of_serial[device.serial].append(device)
            else:
                dict_of_serial[device.serial] = [device]
        
        for serial in dict_of_serial:
            if len(dict_of_serial[serial]) > 1:
                for device in dict_of_serial[serial]:
                    devices = ", ".join([f"{value}" for value in dict_of_serial[serial]])
                    result['log_failure'] = result.get('log_failure', []) + [(device, f"{serial} - the serial number is repeated in these devices ({devices})",  )]

    def post_run(self):
        data = json.dumps(self._results)
        if SEND_EMAIL:
            email_template = Template(EMAIL_FORMAT)
            context = Context(
                {
                    'data': json.loads(data)
                }
            )
            message = email_template.render(context)
            send_mail(
                message=message,
                **EMAIL
            )

